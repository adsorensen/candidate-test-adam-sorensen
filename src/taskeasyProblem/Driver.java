package taskeasyProblem;
import java.io.*;
import com.google.gson.*;

public class Driver {

	public static void main(String[] args) {
		Actress[] data = new Actress[50];
		data = importCSV(data);
		Gson g = new Gson();
		String str = g.toJson(data);
		System.out.println(str);
		
	}
	
	// Method will fill an array of Actresses from a given csv file.
	// Parameters: Actress array
	// Returns: Actress array containing csv data
	public static Actress[] importCSV(Actress[] data)
	{
		String file = "./academy_award_actresses.csv";
		String lineToRead = "";
		BufferedReader br = null;
		int yr;
		try
		{
			br = new BufferedReader(new FileReader(file)); 
			// Skip the first line since it is the header and 
			// has no data.
			lineToRead = br.readLine();
			lineToRead = br.readLine();
			int i = 0;
			while (lineToRead != null) 
			{
				lineToRead = lineToRead.replace("\"","");
				String[] parsedLine = lineToRead.split(",");
				yr = Integer.parseInt(parsedLine[0]);
				data[i] = new Actress(parsedLine[2], parsedLine[1], yr);
				lineToRead = br.readLine();
				i++;
			}
			
		}
		catch (FileNotFoundException e)
		{
			System.out.println(e);
		}
		catch (IOException e)
		{
			System.out.println("IO exception with reading the file.");
		}
		finally
		{
			try
			{
				if (br != null)
					br.close();
			}
			catch (IOException e)
			{
				System.out.println("error closing file");
			}
		}
		return data;
	}

}


