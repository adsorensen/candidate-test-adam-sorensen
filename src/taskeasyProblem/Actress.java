package taskeasyProblem;

// Actress class to hold csv data.
// Actress contains a film string, name string, and integer year data fields.
public class Actress {
	private String film;
	private String name;
	private int year;
	
	// Constructor
	public Actress(String film, String name, int year)
	{
		this.film = film;
		this.name = name;
		this.year = year;
	}
	
	// Get methods
	public int getYear()
	{
		return this.year;
	}
	
	public String getFilm()
	{
		return this.film;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	// Set methods
	private void setYear(int year)
	{
		this.year = year;
	}
	
	private void setFilm(String film)
	{
		this.film = film;
	}
	
	private void setName(String name)
	{
		this.name = name;
	}
}
