// Function to execute on page load
$( document ).ready(function() {
    // Ajax call to the locally served JSON data
    $.ajax({
        url: 'http://127.0.0.1:8000/academy_award_actresses.json',
        type: 'GET',
        contentType: 'application/json',
        
        // Upon success, it will parse the response data into html table rows
        success: function (response) {
            var t;
            
            for (var i = 0; i < response.length; i++)
            {
                // alternating table row declarations that change the color of the row
                if (i % 2 == 0)
                    t = $("<tr id=\"actress_row\" bgcolor=\"#00FF00\" <a href=\"\">");
                else
                    t = $("<tr id=\"actress_row\" bgcolor=\"red\"> <a href=\"\">");

                t.append("<td>" + response[i]["year"] + "</td>");
                t.append("<td>" + response[i]["actress"] + "</td>");
                t.append("<td>" + response[i]["movie"] + "</td>");
                t.append("</a></tr>");
                $("#actress_table").append(t);
            }
            addRowHandlers();
        },
        error: function(data,status,er) {
            alert("error: "+data+" status: "+status+" er:"+er);
        }
    });

    // Tried to do this function for the click handler, but went with another way
    $('#actress_table').click(function(e) {
        // couldn't figure how to grab data from the row that was clicked
    });
});

// Function adds click handlers to each row
function addRowHandlers() {
    // Get the actress html table, then get the html rows.
    var table = document.getElementById("actress_table");
    var rows = table.getElementsByTagName("tr");
    // for loop to iterate over the 48 rows in the table
    for (i = 0; i < rows.length; i++) {
        // get current html table row object
        var currentRow = table.rows[i];
        // create function that can be called to get row data and then alert it
        var createClickHandler = 
            function(row) 
            {
                return function() 
                { 
                    var yr = row.getElementsByTagName("td")[0].innerHTML;
                    var name = row.getElementsByTagName("td")[1].innerHTML;
                    var movie = row.getElementsByTagName("td")[2].innerHTML;
                    
                    alert("Year: " + yr + ", Actress: " + name + ", Movie: " + movie);
                };
            };
        currentRow.onclick = createClickHandler(currentRow);
    }
}   
